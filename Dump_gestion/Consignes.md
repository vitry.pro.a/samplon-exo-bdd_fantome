Dump:
https://gitlab.com/IsabelleM/fantomes.git

# Méthodes:
- Cloner les repo avec les bdd
- Choisir sa bdd préférée
- Mettre la bdd dans mysql
- Faire des fonctions

Trouver tous les cas d'une catégorie
Trouver tous les cas qui contienne un mot dans le titre
Trouver tous les cas qui contienne un mot dans les comments
Idem pour location, date, etc.
Faire une fonction qui peut appeler toues les autres fonctions en fonction de ses paramettres

En plus ce serait bien d'avoir les résultats dans l'ordre, genre du plus pertinent au moins pertinent.

Pour la pertinence : 1 résultat dans l'ordre
- categorie
- titre
- location 
- date time 
- comments

- booleen pour weather dependent
- prioriser la cherche dans le calendrier si un nom de mois est dans l'entré
- avoir un timer pour mesurer le temps de réponse

On veut pour lady grey par exemple
→ les cas des catégories avec "lady grey", que "lady" ou que "grey"
→ les cas avec dans title "lady grey" ou "grey lady" enfin avec les deux mots, puis les cas que "lady" ou que "grey
→ les cas avec dans les autres attributs "lady grey" ou l'inverse, puis que "lady" ou que "grey"
sans doublon