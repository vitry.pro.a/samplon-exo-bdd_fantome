-- Fulltext se fait soit sur 1 colonnne pour la recherche d'une colonne
-- Soit sur plusieurs colonnes pour la recherche sur plusieurs colonnes
-- La duplication du fulltext est inutile car elle ralenti la BDD.

ALTER TABLE `cas` ADD FULLTEXT(`title`);
ALTER TABLE `cas` ADD FULLTEXT(`type`, `location`, `date`, `comments`);
ALTER TABLE `category` ADD FULLTEXT(`name`);

SELECT * FROM `cas` 
WHERE MATCH(`title`) 
AGAINST ('lady grey' IN NATURAL LANGUAGE MODE);

SELECT * FROM `cas` 
WHERE MATCH(`type`, `location`, `date`, `comments`) 
AGAINST ('lady grey' IN NATURAL LANGUAGE MODE);