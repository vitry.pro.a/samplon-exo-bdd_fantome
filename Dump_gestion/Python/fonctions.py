import mysql.connector
from datetime import datetime

connection = mysql.connector.connect(
    database = "para",
    user = "antony",
    password = "choupette"
)
cursor = connection.cursor()

# Trouver tous les cas d'une catégorie
def search_by_category(category: str) -> list:
    sql = """
    select cas.title, cas_cat.cat_id from cas
    join cas_cat on cas_cat.cas_id = cas.id
    and cas_cat.cat_id in (
        select id from category 
        where name like %s
        or parent_id in (   
            select id from category 
            where name like %s
        )
    );
    """
    cursor.execute(sql, [category, category])
    return cursor.fetchall()

# Trouver tous les cas qui contienne un mot dans un champ
"""TABLE cas: `id`/`title`/`type`/`location`/`date`/`comments`/`month`/`weather`"""
def search_by_world_in_field(word: str, field_list: list) -> list:
    champs: list = ["title", "type", "location", "date", "comments"]
    # Requète SQL
    def requete(word: str, field: str, field_str: str) -> list:
        sql = f"""SELECT {field} FROM `cas` WHERE MATCH({field_str}) 
                  AGAINST (%s IN BOOLEAN MODE);"""
        # print(sql)
        cursor.execute(sql, [word])
        return cursor.fetchall()
    # Standardisation des champs pour la requète SQL
    def standard_field(field: list) -> str:
        return "`" + "`, `".join(field) + "`"
    def standard_word(word: str) -> str:
        return '+' + ' +'.join(word.split(" "))

    # Vérification de la présence des champs de la tables cas
    for element in field_list:
        if element not in champs:
            raise ValueError("Invalid field name")
    # Vérification du nombre de champs dans field_list
    if len(field_list) != 1:
        field_select: str = standard_field(champs)
        field_match: str = standard_field(field_list)
        word_str = standard_word(word)
        # print("list != 1", word_str)
        # print("field_select", field_select)
        # print("field_match", field_match)
        resultat: list = requete(word, field_select, field_match)
        return resultat
    else:
        field_select: str = standard_field(champs)
        field_match: str = standard_field(field_list)
        word_str = standard_word(word)
        # print("list == 1", word_str)
        # print("field_select", field_select)
        # print("field_match", field_match)
        resultat: list = requete(word_str, field_select, field_match)
        return resultat

if __name__=="__main__":
    # Test
    # Trouver tous les cas d'une catégorie
    print(search_by_category("South West England"))

    # Trouver tous les cas qui contienne un mot dans le "titre" 
    # ou dans "type", "location", "date", "comments"
    nb_resultat: int | None = 1
    champs_1: list = ["title"]
    champs_2: list = ["type", "location", "date", "comments"]
    word_to_find = ["Man", "black", "Man black", "lady", "red", "Grey", "lady red", "red lady", "Grey Lady", "Lady Grey"]

    for element in word_to_find:
        t = datetime.now()
        arg = champs_1
        test = search_by_world_in_field(element, arg)
        print(element, arg, test[:nb_resultat], len(test), datetime.now() - t)
        print()
        arg = champs_2
        test = search_by_world_in_field(element, arg)
        print(element, arg, test[:nb_resultat], len(test), datetime.now() - t)
        print()

# Note: FULLTEXT ET MATCH AGAINST
# Fulltext se fait soit sur 1 colonnne pour la recherche d'une colonne
# Soit sur plusieurs colonnes pour la recherche sur plusieurs colonnes
# La duplication du fulltext est inutile car elle ralenti la BDD.
# field = "title"
# field_str = "type, location, date, comments"
# SELECT {field} FROM `cas` WHERE MATCH({field_str}) AGAINST (%s IN BOOLEAN MODE);