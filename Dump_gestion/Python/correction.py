import mysql.connector
from datetime import datetime

connection = mysql.connector.connect(
    database = "para",
    user = "antony",
    password = "choupette"
)
cursor = connection.cursor()

def search(text):
    t = datetime.now()
    print("\nsearch ", text)
    req0 = """
        select cas.* from cas
        join cas_cat on cas_cat.cas_id = cas.id
        and cas_cat.cat_id in (
            select id from category 
            where match(name) against (%s in natural language mode)
            or parent_id in (   
                select id from category 
                where match(name) against (%s in natural language mode)
            )
        );
    """
    cursor.execute(req0, (text,text))
    result = set(cursor.fetchall())
    print("category ", len(result), "\t\t\t", datetime.now() - t)
    req1 = """
            SELECT * FROM cas WHERE 
            MATCH(type,location,date,comments) 
            AGAINST (%s IN NATURAL LANGUAGE MODE);
        """
    cursor.execute(req1, (text,))
    result.update(cursor.fetchall())
    print("category + title ", len(result), "\t\t", datetime.now() - t)
    req2 = """
        SELECT * FROM cas WHERE 
        MATCH(title) 
        AGAINST (%s IN NATURAL LANGUAGE MODE);
    """
    cursor.execute(req2, (text,))
    result.update(cursor.fetchall())
    print("category + title + content ", len(result),'', datetime.now() - t)
    return result

search('south east')
search('devon')
search("lady grey")
search("Lady Grey")
search("black")
search("London")
search("ghost")
search("alien")