# si ca marche avec les "sous categories"
# ca marche pas avec celle au dessus (dans l'arbre)
"""
select * from cas
join cas_cat on cas.id = cas_cat.cas_id
join category on cas_cat.cat_id = category.id
where category.name like "category";
"""
# pour que ca marche il faudrai que je trouve...
# 1... toutes les sous catégories de category (le param)
# 2... je cherche tous les cas de toutes les 
# sous categories de la categorie que je cherche.
"""
select cas.title, cas_cat.cat_id from cas
join cas_cat on cas_cat.cas_id = cas.id
and cas_cat.cat_id in (
    select id from category 
    where name like 'South West England'
    or parent_id in (   
        select id from category 
        where name like 'South West England'
    )
);
"""

"""TABLE cas: `id`/`title`/`type`/`location`/`date`/`comments`/`month`/`weather`"""
def search_by_world_in_field(word: str, field: str) -> list:
    if field not in ["title", "type", "location", "date", "comments"]:
        raise ValueError("Invalid field name")
    word_str = '%' + "% %".join(word.split(" ")) + '%'
    print(word_str)
    sql = f"""SELECT {field} FROM cas WHERE {field} LIKE %s;"""
    cursor.execute(sql, [word_str])
    return cursor.fetchall()


"""TABLE cas: `id`/`title`/`type`/`location`/`date`/`comments`/`month`/`weather`"""
def search_by_world_in_field(word: str, field: str) -> list:
    def requete(word_str: str, field: str) -> list:
        sql = f"""SELECT {field} FROM cas WHERE {field} LIKE %s;"""
        cursor.execute(sql, [word_str])
        return cursor.fetchall()
    
    if field not in ["title", "type", "location", "date", "comments"]:
        raise ValueError("Invalid field name")
    
    word_str = '%' + "% %".join(word.split(" ")) + '%'
    print(word_str)
    resultat = requete(word_str, field)
    return resultat