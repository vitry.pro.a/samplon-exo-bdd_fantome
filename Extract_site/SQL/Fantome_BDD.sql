/* Create the database */
DROP DATABASE IF EXISTS fantome_bdd;
CREATE DATABASE fantome_bdd;

/* Switch to the classicmodels database */
USE fantome_bdd;

/* Drop existing tables  */
DROP TABLE IF EXISTS `element`;
DROP TABLE IF EXISTS `categorie`;
DROP TABLE IF EXISTS `element_categorie`;


CREATE TABLE `element` (
  `element_id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `title` varchar(255),
  `location` varchar(255),
  `type` varchar(255),
  `date_time` varchar(255),
  `comments` text,
  `month` int,
  `weather` bool
);

CREATE TABLE `categorie` (
  `categorie_id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `parent` int,
  `name` varchar(255) UNIQUE NOT NULL,
  `cat_type` ENUM ('geographic', 'reports') NOT NULL
);

CREATE TABLE `element_categorie` (
  `element_id` int NOT NULL,
  `categorie_id` int NOT NULL
);

ALTER TABLE `element_categorie` ADD FOREIGN KEY (`element_id`) REFERENCES `element` (`element_id`);

ALTER TABLE `element_categorie` ADD FOREIGN KEY (`categorie_id`) REFERENCES `categorie` (`categorie_id`);