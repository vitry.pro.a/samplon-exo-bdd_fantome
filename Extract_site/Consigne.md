Contexte du projet

On veut créer une base de données facilement intérogeable à partir de du site : https://www.paranormaldatabase.com/

Il y a deux possibilités :
    Copier coller toutes les pages du site.
    Faire un script python qui s’occupe de récupérer le contenu du site et de l’insérer dans une BDD.

Dans tous les cas, il faut dans un premier temps analyser le site pour en déduire son modèle de données.
donc…

1 Faire le modèle de données du site.

Ensuite il faut déterminer la structure de notre base de données en respectant les règles de normalisation.
donc…

​2 Faire le modèle de notre BDD.
(et, bien sur, écrire le code qui permet de la créer)

​Par la suite, on peut copier coller toutes les pages du site et les insérer dans la BDD.

ou

Faire un script qui récupère le contenu de toutes les pages, découpe le DOM (et le texte) et fait les inserts dans la BDD.

​On veut au final faire une carte du Royaume-Uni avec une épingle sur chaque position géographique dans la bdd.

Quand on clique sur l’épingle, affiche les informations sur le cas représenté.