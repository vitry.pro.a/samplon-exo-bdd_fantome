from bs4 import BeautifulSoup
from urllib.request import urlopen
from pprint import pprint
import mysql.connector

#-------------------------------------------#
# Fonctions:                                #
#-------------------------------------------#

def get_page_content(url):
    url_site = "https://www.paranormaldatabase.com"
    html = urlopen(url_site + url).read().decode("utf-8")
    return BeautifulSoup(html, "html.parser")


def get_list_css_class(contenu, balise: str, class_css: str):
    return contenu.find_all(balise, attrs = {'class': class_css})


def format_key_dico(string: str):
    string_text = string.replace(' / ',' ').replace(':','').replace(' ','_').lower()
    return string_text


def format_val_dico(string: str):
    return string.strip()


def extract_link_to_dict(contenu, balise, attr):
    list_dict = []
    divs = get_list_css_class(contenu, balise, attr)
    for ind, element in enumerate(divs):
        dico = {}
        # print(ind, element.find().contents, element.find().name)
        if element.select("h3") != []:
            # H3 => Country et Regions
            dico['title'] = element.h3.getText()
            if element.select("a") != []:
                if not element.find('a', {'name': 'england'}):
                    dico['link'] = element.a.attrs['href']
        elif element.select("h4") != []:
            # H4 => Sub-Regions
            dico['title'] = element.h4.getText()
            if element.select("a") != []:
                dico['link'] = element.a.attrs['href']
        elif element.select("h5") != []:
            dico['title'] = element.h5.getText()
            if element.select("a") != []:
                dico['link'] = element.a.attrs['href']
        # if element.select("a") != []:
        #     if not element.find('a', {'name': 'england'}):
        #         dico['link'] = element.a.attrs['href']
        list_dict.append(dico)
    return list_dict

def extract_cas_to_dict(contenu, balise, attr):
    list_dict, key, val = ([] for i in range(3))
    dico, tmp_dico = ({} for i in range(2))
    divs = get_list_css_class(contenu, balise, attr)
    for element in divs:
        element_content = element.p.contents
        # print(element_content)
        sub_element = [sub.contents for sub in element_content if sub.name != None]
        if sub_element != [] and sub_element[0] != []:
            dico['title'] = sub_element[0][0].string
            for sub in sub_element[1]:
                if sub != '\n' and sub.name != "br":
                    if sub.name == "span":
                        key.append(format_key_dico(sub.string))
                    else:
                        val.append(format_val_dico(sub))
            tmp_dico = dict(zip(key, val))
            dico.update(tmp_dico)
            list_dict.append(dico)
    return list_dict


def get_category_name(contenu):
    # ???
    pass


def insert_bdd_with_return_id(conn, cursor, table: str, column: list, value: list) -> int:
    val = ["%s" for x in range(1, (len(value) + 1))]
    str_val = ", ".join(val)
    column_str = ", ".join(column)
    try:
        sql = f"INSERT INTO {table} ({column_str}) VALUES ({str_val});"
        # print(sql)
        cursor.execute(sql, value)
        conn.commit()
        id = cursor.lastrowid
    except:
        cond_column = "name"
        column = "categorie_id"
        val = [value[0]]
        sql = f"SELECT {column} FROM {table} WHERE {cond_column} = %s;"
        # print(sql)
        cursor.execute(sql, val)
        id = cursor.fetchone()[0]
    return id

#-------------------------------------------#
# Inialisation:                             #
#-------------------------------------------#

# Initialisation de la liaison avec le site à scraper

# Initialisation de la liaison avec la BDD à implémenter
BDD = "fantome_bdd"
mydb = mysql.connector.connect(
  host="localhost",
  user="antony",
  password="choupette",
  database=BDD
)
bdd_info = """
Table: element
    element_id int
    title varchar
    location varchar
    type varchar
    date_time varchar
    comments text
    month int
    weather bool

column = ["categorie_id", "parent", "name", "cat_type"]
Table: categorie
    categorie_id int
    parent int
    name varchar 
    cat_type categorie_type

Table: element_categorie
    element_id int
    categorie_id int

Enum: categorie_type
    geographic
    reports
"""

#Inite du curseur de la BDD
mycursor = mydb.cursor()

# Initialisation des classes CSS par liens
    # Country
attr_country = "w3-card w3-container w3-left-align"
    # Région sauf pour England
attr_regions_1 = "w3-row-padding w3-left-align w3-margin-top w3-card"
attr_regions_2 = "w3-row-padding w3-left-align w3-margin-top w3-border"
    # Sous-Régions sauf pour England
attr_sub_regions = "w3-border-left w3-border-top w3-container w3-left-align"
    # Sous-Régions pour England
attr_sub_regions_1 = "w3-row-padding w3-left-align w3-margin-top w3-card"
attr_sub_regions_2 = "w3-row-padding w3-left-align w3-margin-top w3-border"

#-------------------------------------------#
# Script:                                   #
#-------------------------------------------#
if __name__ == '__main__':

    #=========================================#
    # Récupérer le contenu: index.html
    url_index = "/index.html"
    print("Url index:", url_index)
    print()
    document = get_page_content(url_index)
    #=========================================#

#-------------------------------#
#   Etape 1: Liens des pays     #
#-------------------------------#
    
    #----------------------------------------
    # Récupérer la liste des liens des pays
    #----------------------------------------
    
    list_country = extract_link_to_dict(document, 'div', attr_country)
    title_england = document.find('h3').getText()
    list_country.insert(0, {"title":title_england, "link": url_index})
    # print(list_country)

    #--------------------------------------------
    # Pour chaque élément de la liste pays:
    #--------------------------------------------
    for index, pays in enumerate(list_country):
        # Récupérer le titre du pays
        country = pays["title"]
        # Vérifier et récupérer l'id_country
        id_parent_pays = None
        table = "categorie"
        column = ["name", "parent", "cat_type"]
        value = [country, id_parent_pays, "geographic"]
        print(value)
        id_pays = insert_bdd_with_return_id(mydb, mycursor, table, column, value)
        list_country[index]["id"] = id_pays
        # Affichage du titre et de l'id_parent
        print("Element_country:", list_country[index])

        #-------------------------------------------
        # Récupérer la liste des liens des régions
        #-------------------------------------------

        #=========================================#
        # Récupérer le contenu: Link Country
        url_pays = list_country[index]["link"]
        print("Url pays:", url_pays)
        parent_id = list_country[index]["id"]
        document_pays = get_page_content(url_pays)
        #=========================================#

        # Extract des listes regions/sous-regions des pages html
            # Régions sauf pour England
        list_regions_1 = extract_link_to_dict(document_pays, 'div', attr_regions_1)
        list_regions_2 = extract_link_to_dict(document_pays, 'div', attr_regions_2)
        if list_regions_1[0]['title'] == "England":
            # Suppression du pays England dans Regions
            del(list_regions_1[0])
#---------------------------------------------------------------------------------------------
        if country == "England":
            # Sous-Régions pour England
            list_regions_3 = extract_link_to_dict(document_pays, 'div', attr_sub_regions)           

            for ind_region, reg_england in enumerate(list_regions_3):
                print("For - Region:", index, ind_region, reg_england)
                # Récupérer le titre du pays
                region_england = reg_england["title"]
                # Inserer l'id_parent (pays) dans le dico
                list_regions_3[ind_region]["id_parent"] = id_pays
                # Vérifier et récupérer l'id_country
                id_parent_region = reg_england["id_parent"]
                table = "categorie"
                column = ["name", "parent", "cat_type"] # manque id_parent
                value = [region_england, id_parent_region, "geographic"]
                print("Valeur insertion BDD:", value)
                print()
                id_region = insert_bdd_with_return_id(mydb, mycursor, table, column, value)
                list_regions_3[ind_region]["id"] = id_region
                # Affichage du titre et de l'id_parent
                print("Element_region_england:", list_regions_3[ind_region])

                #===============================================================#
                # Récupérer le contenu: Link Region England
                url_region_england = list_regions_3[ind_region]["link"]
                print("Url region england:", url_region_england)
                parent_id = list_country[index]["id"]
                document_region_england = get_page_content(url_region_england)
                #===============================================================#

                #------------------------------------------------
                # Récupérer la liste des liens des Sous-Régions
                #------------------------------------------------

                list_sub_regions_1 = extract_link_to_dict(document_region_england, 'div', attr_sub_regions_1)
                list_sub_regions_2 = extract_link_to_dict(document_region_england, 'div', attr_sub_regions_2)

                list_sub_regions_1[index]["id_parent"] = id_region
                list_sub_regions_1[index]["region"] = reg_england["title"]
                list_sub_regions_2[index]["id_parent"] = id_region
                list_sub_regions_1[index]["region"] = reg_england["title"]
                
                # print("sub 1", index, list_sub_regions_1)
                # print("sub 2", index, list_sub_regions_2)

                # Regroupement liste: Sous-region
                list_sub_regions = list_sub_regions_1 + list_sub_regions_2

                for ind_sub, sub_england in enumerate(list_sub_regions):
                    print("For - Region:", index, ind_region, ind_sub, sub_england)
                    # Récupérer le titre de sous region
                    england_sub = sub_england["title"]
                    # Inserer l'id_parent (pays) dans le dico
                    list_sub_regions[ind_sub]["id_parent"] = id_region
                    # Vérifier et récupérer l'id_country
                    id_parent_sub = sub_england["id_parent"]
                    table = "categorie"
                    column = ["name", "parent", "cat_type"] # manque id_parent
                    value = [england_sub, id_parent_sub, "geographic"]
                    print("Valeur insertion BDD:", value)
                    print()
                    id_sub_region = insert_bdd_with_return_id(mydb, mycursor, table, column, value)
                    list_sub_regions[ind_sub]["id"] = id_sub_region
                    # Affichage du titre et de l'id_parent
                    print("Element_sub_england:", list_sub_regions[ind_sub])
            print("For end - Region England")
#---------------------------------------------------------------------------------------------
        else:
            # Sous-Régions sauf pour England
            list_sub_regions_1 = extract_link_to_dict(document_pays, 'div', attr_sub_regions)
            # Regroupement liste: Sous-region
            list_sub_regions = list_sub_regions_1
#---------------------------------------------------------------------------------------------
        print()
        # Regroupement liste: Region
        list_regions = list_regions_1 + list_regions_2 + list_regions_3
        list_regions[index]["pays"] = pays["title"]
        print("Regions:", list_regions)
        print()
        print("Sous-Regions:", list_sub_regions)
    print("For end - country:")
    print("Country:", list_country, "Regions:", list_regions, "Sub-Regions:", list_sub_regions)

        #-------------------------------------------
        # Pour chaque élément de la liste Regions:
        #-------------------------------------------

        # for ind_regions, regions in enumerate(list_regions):
        #     # list_regions[ind_regions]["parent_id"] = id
        #     print("Element_region:", regions)

        #----------------------------------------------
        # Pour chaque élément de la liste Sub_Régions:
        #----------------------------------------------
        
        # for ind_sub, sub_region in enumerate(list_sub_regions):
        #     # list_sub_regions[ind_sub]["parent_id"] = id
        #     print("Element_sub_region:", sub_region)
        






    # attr_sub_regions = "w3-border-left w3-border-top w3-container w3-left-align"
    # print()
#-------------------------------#
#   Etape 2: Liens des reports  #
#-------------------------------#
    
    # Récupérer la liste des liens des reports
    # attr_report_1 = "w3-half w3-container w3-left-align"
    # attr_report_2 = "w3-quarter w3-container w3-left-align w3-border-left"
    # attr_report_3 = "w3-quarter w3-container w3-border-left w3-left-align"
    # list_report_1 = extract_link_to_dict(document, 'div', attr_report_1)
    # list_report_2 = extract_link_to_dict(document, 'div', attr_report_2)
    # list_report_3 = extract_link_to_dict(document, 'div', attr_report_3)
    # for dico in list_report_1:
    #     if dico["title"] == "Contribute":
    #         list_report_1.remove(dico)
    # list_report = list_report_1 + list_report_2 + list_report_3
    
    # # # Pour chaque élément de la liste:
    # for element in list_report:
    #     print(element)


    # Test sur une liste de cas
        
    # url_berdata = "/berkshire/berkdata.php"
    # doc_berdata = get_page_content(url_berdata)

  
    # list_cas = extract_cas_to_dict(doc_berdata, 'div', attr_6)
    # pprint(list_cas)



    # Test de code:

    # Récupérer le lien de la régions
    # link = element["link"]
    # print(link)
    # # Récupérer le lien de la régions
    # name_regions = element["title"]
    # print(name_regions)