from bs4 import BeautifulSoup
from urllib.request import urlopen
from pprint import pprint
import mysql.connector

# Initialisation de la liaison avec le site à scraper
url = "https://www.paranormaldatabase.com/"
page = urlopen(url)
html = page.read().decode("utf-8")
document = BeautifulSoup(html, "html.parser")
# print(document)


# print(document.select("a *"))
# balise = document.html
# print("balise", balise)
# print("name", balise.name)
# print("attrs", balise.attrs)
# # print(balise.attrs["href"], balise.attrs["class"])
# print("string", balise.string)
# print("contents", balise.contents)
# print("previous", balise.previous_sibling)
# print("next", balise.next_sibling)
# # print("parent", balise.parent)
# print("parent", balise.parent.name)
# # print("enfant", balise.children)
# for child in balise.children: 
#     if child.name != None:
#         print("enfant", child.name)
#         # print("enfant", child)

list_arbre_dom = {}

def arbre_dom(page_Html, list_dom):
    document = page_Html
    list_child = []
    list_parent = []
    def parent(balise):
        if balise.parent != None:
            print("Fonction parent:", balise.parent.name)
            list_parent.append(balise.parent.name)
        else:
            print("Pas de parent touvé:", balise.parent)
    def enfant(balise):
        temp = []
        for child in balise.children:
            if child.name != None:
                print("Fonction enfant:", child.name)
                list_child.append(child.name)
                temp.append(child.name)
        if temp != [] and len(temp) > 1:
            if list_parent != []:
                list_dom[page_Html.name] = temp
            else:
                list_dom[document.name[1:-1]] = temp
        elif temp != [] and len(temp) == 1:
            if list_parent != []:
                list_dom[page_Html.name] = temp[0]
            else:
                list_dom[document.name[1:-1]] = temp[0]
    parent(page_Html)
    enfant(page_Html)
    print("Arbre DOM: ", list_parent, "->", page_Html.name, "->", list_child)
    for i in range(0, len(list_child)):
        next_balise = document.find(list_child[i])
        print()
        print("Nouvelle Branche:", next_balise.name)
        arbre_dom(next_balise, list_dom)


# Vérification pour Document
# print("Parent / Enfant:", document.name)
# arbre_dom(document, list_arbre_dom)
# print()
# print(list_arbre_dom)

# print(list_arbre_dom["footer"])
# print(document.select(list_arbre_dom["footer"][0]))
# print(document.select(list_arbre_dom["footer"][1]))
# print(document.find(list_arbre_dom["footer"][0]).contents)

# regions/southeast.html
attr_1 = "w3-row-padding w3-left-align w3-margin-top w3-card" # Box england contenant attr_2
# index.html#england
attr_2 = "w3-border-left w3-border-top w3-container w3-left-align" # Box liste les éléments htmtl

attr_3 = "w3-row-padding w3-left-align w3-margin-top w3-border"
#
attr_4 = "w3-border-left w3-border-top w3-left-align"
# cornwall/pages/corndata.php
attr_5 = "w3-border-left w3-border-top w3-left-align"


divs = document.find_all('div', attrs = {'class': attr_2})
for ind, element in enumerate(divs):
    # print(ind, element)
    if element.select("h4") != []:
        print("h4 =>", element.h4.getText())
    if element.select("a") != []:
        print("a =>", element.a.attrs['href'])
    if element.select("p") != []:
        print("p =>", element.p.getText())
    if element.select("img") != []:
        print("img =>", element.img['src'])
    print()


# Code BS4:
    # Var_BS4 pour la version HTML
# all_products_HTML = document.select("div.product")

#     # Var pour le tableau
# all_products_dict = []

#     # Boucle FOR
# for prod_HTML in all_products_HTML:
#         # Var pour le dictionnaire
#     produit = {}
#         # Var pour le nom
#     produit["name"] = prod_HTML.select(".product-info h2")[0].text
#         # Var pour la ref
#     produit["ref"] = prod_HTML.select(".product-info small")[0].text
#         # Var pour la description
#     produit["description"] = prod_HTML.select(".product-info p")[0].text
#         # Var pour le prix réduit
#     produit["price"] = prod_HTML.select(".product-info .line strong.price")[0].text
#         # Var pour le prix initial
#     produit["price_less"] = prod_HTML.select(".product-info .line strong.price")[1].text
#         # Ajouts du dictionnaire dans le tableau
#     produit["discount"] = 0
#     all_products_dict.append(produit)



# Full Texte Fonction

# def full_text(e):
#     if e.string:
#         return e.string
#     else:
#         return "".join(full_text(i) for i in e.contents)

# print(full_text(document))
    


# Extract info:
    # Chemin: div > p > p(1) > H4
        # H4 -> title
    # Chemin: div > p > p(2) > span(1)
        # Nom du champs + Location
    # Chemin: div > p > p(2) > span(2)
        # Nom du champs + Type
    # Chemin: div > p > p(2) > span(3)
        # Nom du champs + Date / Time
    # Chemin: div > p > p(2) > span(4)
        # Nom du champs + Further Comments:
    



# regions/southeast.html
attr_1 = "w3-row-padding w3-left-align w3-margin-top w3-card" # Box england contenant attr_2
# index.html#england
attr_2 = "w3-border-left w3-border-top w3-container w3-left-align" # Box liste les éléments htmtl
#
attr_3 = "w3-row-padding w3-left-align w3-margin-top w3-border"
#
attr_4 = "w3-border-left w3-border-top w3-left-align"
# cornwall/pages/corndata.php
attr_5 = "w3-border-left w3-border-top w3-left-align"
# /berkshire/berkdata.php
attr_6 = "w3-border-left w3-border-top w3-left-align"
# Box contenant un lien d'une Country
attr_8 = "w3-card w3-container w3-left-align"