from bs4 import BeautifulSoup
from urllib.request import urlopen
from pprint import pprint
import mysql.connector

# Initialisation de la liaison avec le site à scraper
url = "https://www.paranormaldatabase.com/"
page = urlopen(url)
html = page.read().decode("utf-8")
document = BeautifulSoup(html, "html.parser")
# print(document)

list_attr = {}
for tag in document.find_all():
    print(tag.name) # récup les nom de balise
    print(tag.attrs) # récup les attribut sous forme dico
    for ind, attr in enumerate(tag.attrs):
        print(ind+1, attr, tag[attr])
        list_attr[attr] = tag[attr]  # récup TOUS les attribut sous forme dico
    print(list_attr)

# list_balise = []
# balise_attr = {}
# for tag in document.find_all():
#     print(tag.name) # récup les nom de balise
#     print(tag.attrs) # récup les attribut sous forme dico
#     if tag.attrs != {}:
#         balise_attr[tag.name] = tag.attrs
#         list_balise.append(balise_attr)
# print(list_balise)


# Version précédente
list_arbre_dom = []

def arbre_dom(page_Html, list_dom):
    list_child = []
    list_parent = []
    def parent(balise):
        if balise.parent != None:
            print("Le parent est:", balise.parent.name)
            list_parent.append(balise.parent.name)
        else:
            print("Pas de parent touvé:", balise.parent)
    def enfant(balise):
        temp = []
        for child in balise.children:
            if child.name != None:
                print("L'enfant est:", child.name)
                list_child.append(child.name)
                temp.append(child.name)
        if temp != [] and len(temp) > 1:
            list_dom.append(temp)
        elif temp != [] and len(temp) == 1:
            list_dom.append(temp[0])
    parent(page_Html)
    enfant(page_Html)
    print("Liste enfant DOM:", list_child)
    print("Liste parent DOM:", list_parent)
    for i in range(0, len(list_child)):
        next_balise = document.find(list_child[i])
        print()
        print("Nouvelle fonction récursive:", next_balise.name)
        arbre_dom(next_balise, list_dom)


# Vérification pour Document
print("Parent / Enfant:", document.name)
arbre_dom(document, list_arbre_dom)
print()
print(list_arbre_dom)

# Version finale
def arbre_dom(page_Html, list_dom):
    list_child = []
    list_parent = []
    def parent(balise):
        if balise.parent != None:
            print("Fonction parent:", balise.parent.name)
            list_parent.append(balise.parent.name)
        else:
            print("Pas de parent touvé:", balise.parent)
    def enfant(balise):
        temp = []
        for child in balise.children:
            if child.name != None:
                print("Fonction enfant:", child.name)
                list_child.append(child.name)
                temp.append(child.name)
        if temp != [] and len(temp) > 1:
            if list_parent != []:
                list_dom[page_Html.name] = temp
                # print("len > 1 et list_parent non vide", list_dom)
            else:
                list_dom[document.name] = temp
                # print("len > 1 et list_parent vide", list_dom)
        elif temp != [] and len(temp) == 1:
            if list_parent != []:
                list_dom[page_Html.name] = temp[0]
                # print("len = 1 et list_parent non vide", list_dom)
            else:
                list_dom[document.name] = temp[0]
                # print("len > 1 et list_parent vide", list_dom)
    parent(page_Html)
    enfant(page_Html)
    # print("Liste parent DOM:", list_parent)
    # print("Balise en cours:", page_Html.name)
    # print("Liste enfant DOM:", list_child)
    print("Arbre DOM: ", list_parent, "->", page_Html.name, "->", list_child)
    for i in range(0, len(list_child)):
        next_balise = document.find(list_child[i])
        print()
        print("Nouvelle Branche:", next_balise.name)
        arbre_dom(next_balise, list_dom)