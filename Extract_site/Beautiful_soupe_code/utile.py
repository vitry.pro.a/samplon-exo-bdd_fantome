def connexion_bdd(connector, list_of_bdd: list):
    """Créer un dictionnaire avec les différentes connections
    Args:
        connector (_type_): connector
        list_of_bdd (list): liste des noms des databases à connecté
    Returns:
        _type_: dictionnaire avec les différentes connections
    """
    db_dict = {}
    for NAME in list_of_bdd:
        db_dict[NAME] = connector.connect(host="localhost", user="antony", 
                                                password="choupette", database=NAME)
    return db_dict


def select_bdd(cursor, table: str, column: str):
    """Interroge la BDD
    Args:
        cursor (_type_): _description_
        table (str): _description_
        column (str): _description_
    Returns:
        _type_: Resultat
    """
    cursor.execute(f"SELECT {column} FROM {table};")
    return cursor.fetchall()


def select_bdd_with_cond(cursor, table: str, column: str, value: list, cond_column: str):
    """Interroge la BDD avec une condition
    Args:
        cursor (_type_): _description_
        table (str): _description_
        column (str): _description_
        value (list): _description_
        cond_column (str): _description_
    Returns:
        _type_: Resultat
    """
    sql = f"SELECT {column} FROM {table} WHERE {cond_column} = %s;"
    cursor.execute(sql, value)
    return cursor.fetchall()


def insert_bdd(conn, cursor, BDD: str, table: str, column: str, value: list):
    """Insert dans la BDD
    Args:
        conn (_type_): _description_
        cursor (_type_): _description_
        BDD (str): _description_
        table (str): _description_
        column (str): _description_
        value (list): _description_
    """
    sql = f"INSERT INTO {BDD}.{table} ({column}) VALUES (%s);"
    cursor.execute(sql, value)
    conn.commit()