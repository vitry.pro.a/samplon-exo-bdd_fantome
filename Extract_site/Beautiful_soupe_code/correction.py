from urllib.request import urlopen
from bs4 import BeautifulSoup
import mysql.connector

def get_page_content(url):
    url = url.replace("..", "")
    return urlopen("https://www.paranormaldatabase.com"+url).read()

def insert_bdd_with_return_id(conn, cursor, table: str, column: list, value: list) -> int:
    val = ["%s" for x in range(1, (len(value) + 1))]
    str_val = ", ".join(val)
    column_str = ", ".join(column)
    try:
        sql = f"INSERT INTO {table} ({column_str}) VALUES ({str_val});"
        # print(sql)
        cursor.execute(sql, value)
        conn.commit()
        # print("Insert")
        id = cursor.lastrowid
    except:
        cond_column = column[:3]
        column = "element_id"
        val = value[:3]
        sql = f"""SELECT {column} FROM {table} 
                    WHERE {cond_column[0]} = %s
                    AND {cond_column[1]} = %s
                    AND {cond_column[2]} = %s;"""
        # print(sql)
        # print(value[:2])
        cursor.execute(sql, val)
        # print("Select")
        id = cursor.fetchone()[0]
    return id

# Initialisation de la liaison avec la BDD à implémenter
BDD = "fantome_correction"
mydb = mysql.connector.connect(
  host="localhost",
  user="antony",
  password="choupette",
  database=BDD
)

#Inite du curseur de la BDD
mycursor = mydb.cursor()

bdd_info = """
Table: element
    element_id int
    title varchar
    location varchar
    type varchar
    date_time varchar
    comments text
    month int
    weather bool

column = ["categorie_id", "parent", "name", "cat_type"]
Table: categorie
    categorie_id int
    parent int
    name varchar 
    cat_type categorie_type

Table: element_categorie
    element_id int
    categorie_id int

Enum: categorie_type
    geographic
    reports
"""

# get content home
html_home = get_page_content("/index.html")

# get category home
list_cat_home = []

soup = BeautifulSoup(html_home, features="html.parser")

liste_thirds = soup.find_all(class_="w3-third")
for third in liste_thirds:
    lien = third.find('a')
    url= lien.get('href')
    list_cat_home.append(url)

liste_halfs = soup.find_all(class_="w3-half")
for third in liste_halfs:
    lien = third.find('a')
    url= lien.get('href')
    list_cat_home.append(url)

liste_q = soup.find_all(class_="w3-quarter")
for third in liste_q:
    lien = third.find('a')
    url= lien.get('href')
    list_cat_home.append(url)


for cat in list_cat_home:
    html_cat = get_page_content(cat)
    soup = BeautifulSoup(html_cat, features="html.parser")
    titre_cat = soup.select_one(".w3-panel > h4:nth-child(1)")
    titre_cat.find("a").extract()
    titre_cat = titre_cat.getText()
    titre_cat = titre_cat.replace("> ", "")
    titre_cat = titre_cat.strip()

    list_sous_cat = []

    liste_thirds = soup.find_all(class_="w3-third")
    for third in liste_thirds:
        lien = third.find('a')
        url= lien.get('href') 
        # Erreur sur la methode get lorsque 
        # link: /regions/otherregions.html /channelislands/jersey.php?
        list_sous_cat.append(url)

    liste_halfs = soup.find_all(class_="w3-half")
    for third in liste_halfs:
        lien = third.find('a')
        if cat == "/regions/otherregions.html":# and ss_cat == "/channelislands/jersey.php":
            print(lien, third)
        url = lien.get('href')
        list_sous_cat.append(url)
    
    print(list_cat_home)
    print(list_sous_cat)

    for ss_cat in list_sous_cat:
        html_sous_cat = get_page_content(ss_cat)
        soup = BeautifulSoup(html_sous_cat, features="html.parser")
        # prendre plutot le titre h3 de la page des cas
        titre_ss_cat = soup.select_one(".w3-panel > h4:nth-child(1)")
        titre_ss_cat.find("a").extract()
        titre_ss_cat.find("a").extract()
        titre_ss_cat = titre_ss_cat.getText()
        titre_ss_cat = titre_ss_cat.replace("> ", "")
        titre_ss_cat = titre_ss_cat.strip()

        # refaire tout pour chaque page de la pagination
        qs = "?"
        while(qs):
            html_sous_cat = get_page_content(ss_cat+qs)
            soup = BeautifulSoup(html_sous_cat, features="html.parser")

            print("link:", cat, ss_cat+qs)
            list_cas = soup.select("div.w3-panel > .w3-half > .w3-border-left.w3-border-top.w3-left-align > p")
                                  # div.w3-panel:nth-child(4) > .w3-half > .w3-border-left.w3-border-top.w3-left-align > p")
            
            for cas in list_cas:
                # parser le cas
                titre_cas = cas.select_one("h4").get_text()
                # recup le fameux P
                p = cas.select_one("p:nth-last-child(1)")
                p_text = p.get_text().split("\n")
                # location varchar(255),
                p_text[0] = p_text[0].replace("Location: ", "")
                # type varchar(255),
                p_text[1] = p_text[1].replace("Type: ", "")
                # date varchar(255),
                p_text[2] = p_text[2].replace("Date / Time: ", "")
                # comments varchar(255),
                p_text[3] = p_text[3].replace("Further Comments: ", "")
                
                if titre_cat == "Calendar":
                    pass
                    # """ month int
                    #     weather bool"""
                    # month dans la cat calendar
                    # weather dans la cat calencar

                # Insertion Data
                table = "element"
                column = ["title", "location", "type", "date_time", "comments"]
                value = [titre_cas, p_text[0], p_text[1], p_text[2], p_text[3]]
                id_cas = insert_bdd_with_return_id(mydb, mycursor, table, column, value)
                # print(id_cas, column)
                # print(id_cas, value)
                print(id_cas, qs)
                print()

                qs = None
            # next
            nav_btn = soup.select(".w3-quarter.w3-container h5")
            for btn in nav_btn:
                if cat == "/regions/otherregions.html":# and ss_cat == "/channelislands/jersey.php":
                    print(btn)
                print(btn.get_text(), qs)
                if btn.get_text() == "next": #.find('next') > 0
                    qs = btn.select_one("a").get("href")
                else:
                    qs = False
                        
