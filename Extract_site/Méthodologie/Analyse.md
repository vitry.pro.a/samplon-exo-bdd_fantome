# Méthodologie

## Analyse de la stucture du site

- Home > Contribute
- Home > England
    - England > South East
    - England > South West
    - England > Greater London
    - England > East of England
    - England > East Midlands
    - England > West Midlands
    - England > The North West
    - England > The North East & Yorkshire
    - Scotland
    - Wales
    - Northern Ireland
    - Republic of Ireland
    - Other Regions
    - Reports: Browse Places
    - Reports: Browse Type
    - Reports: Browse People
    - Contribute
    - The Paranormal Calendar
    - Recent Additions
    - Bibliography
    - Social Media
        - Twitter
        - Facebook
        - Instagram
- Home > Scotland
    - Highlands > Aberdeenshire
    - Highlands > Argyll & Bute
    - Highlands > Highland
    - Highlands > Inverclyde
    - Islands > Orkney
    - Islands > Outer Hebrides
    - Islands > Shetland
    - Lowlands > Angus
    - Lowlands > Ayrshire
    - Lowlands > Borders
    - Lowlands > Clackmannanshire
    - Lowlands > Dumbartonshire
    - Lowlands > Dumfries and Galloway
    - Lowlands > Falkirk
    - Lowlands > Fife
    - Lowlands > Lanarkshire
    - Lowlands > Lothian
    - Lowlands > Moray
    - Lowlands > Perth and Kinross
    - Lowlands > Renfrewshire
    - Lowlands > Stirling
    - Places of Note > Aberdeen
    - Places of Note > Edinburgh
    - Places of Note > Glasgow
- Home > Wales
    - Preserved counties > Clwyd
    - Preserved counties > Dyfed
    - Preserved counties > Gwent
    - Preserved counties > Gwynedd
    - Preserved counties > Mid Glamorgan
    - Preserved counties > Powys
    - Preserved counties > South Glamorgan
    - Preserved counties > West Glamorgan
- Home > Northern Ireland
    - Counties > Co Antrim
    - Counties > Co Armagh
    - Counties > Co Down
    - Counties > Co Fermanagh
    - Counties > Co Londonderry
    - Counties > Co Tyrone
- Home > Republic of Ireland
    - Counties > Co Carlow
    - Counties > Co Cavan
    - Counties > Co Clare
    - Counties > Co Cork
    - Counties > Co Donegal
    - Counties > Co Dublin
    - Counties > Co Galway
    - Counties > Co Kerry
    - Counties > Co Kildare
    - Counties > Co Kilkenny
    - Counties > Co Laois
    - Counties > Co Leitrim
    - Counties > Co Limerick
    - Counties > Co Longford
    - Counties > Co Louth
    - Counties > Co Mayo
    - Counties > Co Meath
    - Counties > Co Monaghan
    - Counties > Co Offaly
    - Counties > Co Roscommon
    - Counties > Co Sligo
    - Counties > Co Tipperary
    - Counties > Co Waterford
    - Counties > Co Westmeath
    - Counties > Co Wexford
    - Counties > Co Wicklow
- Home > Reports: Types
    - Page non recencés
- Home > Reports: Places
    - Page non recencés
- Home > Reports: People
    - Page non recencés
- Home > Calendar
    - Janvier
    - Février
    - Mars
    - Avril
    - Mai
    - Juin
    - Juillet
    - Aout
    - Septembre
    - Octobre
    - Novembre
    - Décembre
- Home > Recent Additions
- Home > About the Author
- Home > Scearch Site (Google)
- Home > Links
- Home > Legal

![](Image_MD/structure_example.png)
![](Image_MD/structure_example_bis.png)


## Analyse des liens du site (<a>Liens</a>)

![](Image_MD/structure_url.png)
![](Image_MD/structure_url_bis.png)

## Analyse des div CSS contenant les éléments Html

### index.html#england

#### Titre de la page
> attr_1 = "w3-row-padding w3-left-align w3-margin-top w3-card"

    - Box england contenant attr_7

#### Sous-Titre de la page
> attr_2 = "w3-row-padding w3-center w3-margin-top"
> attr_3 = "w3-row-padding w3-center w3-margin-top"

    - Box Country/Region

> attr_4 = "w3-row w3-border"

    - Box Reports

> attr_5 = "w3-row w3-border"

    - Box contribute et Calendar

> attr_6 = "w3-row w3-border"

    - Box Recent Additions / Bibliography / Social Media

#### Liste de liens de la page

> attr_7 = "w3-border-left w3-border-top w3-container w3-left-align"

    - Box contenant un lien d'une région
En listant ces Box, on obtient toutes les régions

> attr_8 = "w3-card w3-container w3-left-align"

    - Box contenant un lien d'une Country
En listant ces Box, on obtient toutes les country

### regions/southeast.html

#### Titre de la page
> attr_9 = "w3-panel w3-border-left w3-border-top"

    - Box du titre: nom de la Country

#### Sous-Titre de la page
> attr_10 = "w3-row-padding w3-left-align w3-margin-top w3-card"
> attr_11 = "w3-row-padding w3-left-align w3-margin-top w3-border"

    - Box du titre: nom de la Region

#### Liste de liens de la page
> attr_12 = "w3-border-left w3-border-top w3-container w3-left-align"

    - Box contenant un lien d'une Sous-Region
En listant ces Box, on obtient toutes les Sous-Region

### /berkshire/berkdata.php

#### Titre de la page
> attr_13 = "w3-panel w3-border-left w3-border-top"

    - Box du titre: nom de Country et de la Region

#### Sous-Titre de la page
> attr_14 = "w3-panel w3-border-left w3-border-top"

    - Box du titre: nom de la Region et du type de data

#### Liste des dataq de la page
> attr_15 = "w3-border-left w3-border-top  w3-left-align"

    - Box contenant une data
En listant ces Box, on obtient toutes les datas

## Autres Regions Idem

## Analyse des cas - Champs

<div class="w3-border-left w3-border-top w3-left-align">
    <p>
        <p>
            <h4>
                <span class="w3-border-bottom">
                    Eliza Kleininger
                </span>
            </h4>
        </p>
        <p>
            <span class="w3-border-bottom">Location:</span>
            Ascot - Berystede Hotel, Bagshot Road - Rooms 306, 361, 362<br/>
            <span class="w3-border-bottom">Type:</span> 
            Haunting Manifestation<br/>
            <span class="w3-border-bottom">Date / Time:</span> 
            Twentieth century<br/>
            <span class="w3-border-bottom">Further Comments:</span> 
            Killed in the 1886 fire that destroyed most of the building, 
            Eliza has returned to hunt for the jewellery lost in the blaze.
        </p>
    </p>
</div>

Extract info:
<br/> retour à la ligne
    - Chemin: div > p > p(1) > H4
        - H4 -> title
    - Chemin: div > p > p(2) > span(1)
        - Nom du champs + Location
    - Chemin: div > p > p(2) > span(2)
        - Nom du champs + Type
    - Chemin: div > p > p(2) > span(3)
        - Nom du champs + Date / Time
    - Chemin: div > p > p(2) > span(4)
        - Nom du champs + Further Comments: